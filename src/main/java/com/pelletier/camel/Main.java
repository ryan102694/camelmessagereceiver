package com.pelletier.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**Begins camel context for routes defined in spring.xml
 * 
 * @author Ryan Pelletier
 *
 */
public class Main {
	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		CamelContext camelContext = new DefaultCamelContext();
		try{
			camelContext.start();
			System.out.println("Camel Context Started");
			Thread.sleep(1000000000);
			camelContext.stop();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("Camel Context Stopped");
	}
}
