package com.pelletier.camel.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Prints the stack trace of an exception on an exchange
 * 
 * @author Ryan Pelletier
 */
public class ErrorProcessor implements Processor {

	public void process(Exchange exchange) throws Exception {
		exchange.getException().printStackTrace();
	}

}
