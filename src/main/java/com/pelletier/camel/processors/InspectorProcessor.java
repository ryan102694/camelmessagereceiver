package com.pelletier.camel.processors;

import java.util.Map;
import java.util.UUID;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.pelletier.camel.sandbox.Inspector;

/**
 * Examines a Camel message, uses inspectors to pull out a String from the message body and put
 * in a header.
 * 
 * @author Ryan Pelletier
 *
 */
public class InspectorProcessor implements Processor {
	Map<String,Inspector> inspectors;

	/**
	 * Runs all inspectors in map of inspectors, a new header is created,
	 * the header's name is the key from the map, and its value is what the
	 * inspector returned
	 */
	public void process(Exchange exchange) throws Exception {
		byte[] body = exchange.getIn().getBody().toString().getBytes();

		for(String key : inspectors.keySet()){
			if(inspectors.get(key).inspect(body) != null){
				exchange.getIn().setHeader(key, inspectors.get(key).inspect(body));
			}
		}
	}

	public void setInspectors(Map<String, Inspector> inspectors) {
		this.inspectors = inspectors;
	}
}
