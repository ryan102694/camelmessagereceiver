package com.pelletier.camel.sandbox;

import java.util.UUID;
/**
 * Decorates the injected inspector, allowing it to default to a value if it returns null.
 * @author Ryan Pelletier
 *
 */
public class DefaultValueInspector implements Inspector {
	
	/**
	 * The inspector to decorate.
	 */
	private Inspector inspector = null;

	public String inspect(byte[] message) {
		String value = inspector.inspect(message);
		if( value == null)
		{
			value = UUID.randomUUID().toString();
		}
		
		return value;
	}
		
	public DefaultValueInspector(Inspector inspector) {
		this.inspector = inspector;
	}

}
