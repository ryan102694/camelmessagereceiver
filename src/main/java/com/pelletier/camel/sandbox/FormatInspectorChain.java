package com.pelletier.camel.sandbox;

import java.util.List;
/**
 * Chain of Inspector objects, runs inspectors until one
 * finds a match, then returns the set format from the inspector
 * that matched.
 * 
 * @author Ryan Pelletier
 */
public class FormatInspectorChain implements Inspector {

	private List<Inspector> formatInspectors = null;

	/**
	 * @param message
	 * inspected by list of inspectors for a regex pattern
	 */
	public String inspect(byte[] message) {
		
		String format = null;	
		
		for (Inspector inspector : formatInspectors) {
			format = inspector.inspect(message);
			if (format != null)
				break;
		}
		return format;
	}

	public void setFormatInspectors(List<Inspector> formatInspectors) {
		this.formatInspectors = formatInspectors;
	}

}
