package com.pelletier.camel.sandbox;

/**
 * Examines a byte[], uses some form of message to extract or resolve a value from that byte[]
 * @author Ryan Pelletier
 */
public interface Inspector {
	public String inspect(byte[] message);
}
