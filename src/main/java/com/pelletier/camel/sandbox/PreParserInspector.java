package com.pelletier.camel.sandbox;

import com.bah.pl.messaging.fxml.transform.DomainPreParser;

/**
 * Uses an injected DomainPreParser to attempt to find the format of a given message.
 * @author Ryan Pelletier
 */
public class PreParserInspector implements Inspector {
	DomainPreParser domainPreParser = null;
	
	public String inspect(byte[] message) {
		return domainPreParser.preParseMessage(new String(message));
	}

	public void setDomainPreParser(DomainPreParser domainPreParser) {
		this.domainPreParser = domainPreParser;
	}

}
