package com.pelletier.camel.sandbox;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Uses a regular expression to inspect a byte[] for a certain pattern.
 * @author Ryan Pelletier
 *
 */
public class RegexFormatInspector implements Inspector {

	/**
	 * Returned when the regex pattern finds a match.
	 */
	private String format = null;
	/**
	 * The regex pattern used to find a match in the message.
	 */
	private Pattern pattern = null;	
	
	/**
	 * Converts message to String, runs the regex from pattern
	 * against the message string. If a match is found, format is returned.
	 * 
	 * @return 
	 * The format found.
	 */
	public String inspect(byte[] message) {
		String result = null;
		Matcher matcher = pattern.matcher(new String(message));
		if (matcher.find()) {
			result = format;
		}

		return result;
	}

	public void setPattern(String regex) {
		pattern = Pattern.compile(regex);
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
