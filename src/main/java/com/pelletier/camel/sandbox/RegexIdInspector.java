package com.pelletier.camel.sandbox;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Groups a match from a regular expression, returns specified group of certain index value.
 * @author Ryan Pelletier
 */

public class RegexIdInspector implements Inspector {
	
	/**
	 * The number of the group that is returned from the regex match.
	 * This will default to 1.
	 */
	private int groupNumber = 1;
	/**
	 * The regex pattern to use.
	 */
	private Pattern pattern = null;
	
	/**
	 * Converts message to String, runs regex specified by pattern.
	 * @return One of the regex matches, specified by groupNumber.
	 */
	public String inspect(byte[] message) {
		String result = null;
		Matcher matcher = pattern.matcher(new String(message));
		if (matcher.find()) {
			result = matcher.group(groupNumber);
		}

		return result;
	}

	public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}
	
	public void setPattern(String regex) {
		pattern = Pattern.compile(regex);
	}


}
