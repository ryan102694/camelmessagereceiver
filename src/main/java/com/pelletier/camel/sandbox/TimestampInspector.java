package com.pelletier.camel.sandbox;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.InitializingBean;
/**
 * Measures time between Camel messages, if time is longer than maxTimeBetweenMessages, then 
 * files are saved in directory with a new date header. 
 * @author Ryan Pelletier
 */
public class TimestampInspector implements Inspector, InitializingBean {

	private Date testBeginTime = new Date();	//the date put in the header
	private Date previousMessageReceivedTime = new Date();
	SimpleDateFormat simpleDateFormat; 	
	private long maxTimeBetweenMessages = 600000;
	
	/**
	 * @return
	 * Returns the specified SimpleDateFormat pattern of the testBeginTime date
	 */
	public String inspect(byte[] message) {
		
        Date currentMessageReceivedTime = new Date();
		if(currentMessageReceivedTime.getTime() - previousMessageReceivedTime.getTime() > maxTimeBetweenMessages){
			testBeginTime = currentMessageReceivedTime;
		}
		previousMessageReceivedTime = currentMessageReceivedTime;

		return simpleDateFormat.format(testBeginTime);
	}

	
	public void setMaxTimeBetweenMessages(long maxTimeBetweenMessages) {
		this.maxTimeBetweenMessages = maxTimeBetweenMessages;
	}


	public void setSimpleDateFormatPattern(String simpleDateFormatPattern) {
		 simpleDateFormat = new SimpleDateFormat(simpleDateFormatPattern);
	}
	
	public Date getTestBeginTime() {
		return testBeginTime;
	}


	public Date getPreviousMessageReceivedTime() {
		return previousMessageReceivedTime;
	}


	public long getMaxTimeBetweenMessages() {
		return maxTimeBetweenMessages;
	}

	public void afterPropertiesSet() throws Exception {
		if(simpleDateFormat == null){
			simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_hhmm-aaa");
		}
	}
}
