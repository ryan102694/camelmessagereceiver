package com.pelletier.camel.utils;

import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.InitializingBean;
/**
 * 
 * @author Ryan Pelletier
 * InitializingBean used to create the ActiveMQBroker for accepting JMS messages
 */

public class ActiveMQBrokerUtil implements InitializingBean {
	/**
	 * The port the jms broker will listen on
	 */
	private Integer port;
	
	public void afterPropertiesSet() throws Exception {
		BrokerService broker = new BrokerService();		 
		try{
			broker.addConnector("tcp://localhost:" + port.toString());			 
			broker.start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
}
