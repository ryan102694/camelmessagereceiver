package com.pelletier.camel.sandbox;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.pelletier.camel.sandbox.FormatInspectorChain;
import com.pelletier.camel.sandbox.Inspector;
import com.pelletier.camel.sandbox.RegexFormatInspector;

import static org.junit.Assert.assertEquals;

public class FormatInspectorChainTest {
	
	Inspector dsscsFormatInspector;
	Inspector nomFormatInspector;
	Inspector socommFormatInspector;
	FormatInspectorChain formatInspectorChain;
	String format = null;
	
	@Before
	public void setupChain(){
		//create format inspectors, id inspectors not put in FormatInspectorChain
		Inspector dsscsFormatInspector = new RegexFormatInspector();
		((RegexFormatInspector) dsscsFormatInspector).setPattern("(FM )");
		((RegexFormatInspector) dsscsFormatInspector).setFormat("DSSCS");
		
		Inspector nomFormatInspector = new RegexFormatInspector();
		((RegexFormatInspector) nomFormatInspector).setPattern("(gov:ic:nro:om)");
		((RegexFormatInspector) nomFormatInspector).setFormat("NOM");

		
		Inspector socommFormatInspector = new RegexFormatInspector();
		((RegexFormatInspector) socommFormatInspector).setPattern("(BT)");
		((RegexFormatInspector) socommFormatInspector).setFormat("SOCOMM");
		
		//create list of FormatInspectors
		List<Inspector> inspectorList = new ArrayList<Inspector>();
		inspectorList.add(socommFormatInspector);
		inspectorList.add(dsscsFormatInspector);
		inspectorList.add(nomFormatInspector);

		formatInspectorChain = new FormatInspectorChain();
		formatInspectorChain.setFormatInspectors(inspectorList);
		
	}
	
	@Test
	public void testNomFormat() throws IOException{
		format = formatInspectorChain.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/nom.xml"))));
		assertEquals("NOM", format);			
	}
	
	@Test
	public void testDsscsFormat() throws IOException{
		format = formatInspectorChain.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/dsscs.txt"))));
		assertEquals("DSSCS", format);	
	}
	
	@Test
	public void testSocommFormat() throws IOException{
		format = formatInspectorChain.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/socomm.txt"))));
		assertEquals("SOCOMM", format);
	}
}
