package com.pelletier.camel.sandbox;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.pelletier.camel.sandbox.RegexFormatInspector;

public class RegexFormatInspectorTest {
	
	RegexFormatInspector inspector;
	String format;

	@Before
	public void setup(){
		format = null;
		inspector = new RegexFormatInspector();
	}
	
	@Test
	public void testInspectLine() throws IOException {
		//we will need to fix these
		inspector.setPattern("(sentence)");
		inspector.setFormat("SENTENCE");
		format = inspector.inspect("is there a snetnece in here".getBytes());
		assertEquals(null, format);
	}
	
	@Test
	public void testInspectDsscs() throws IOException {
		inspector.setPattern("(FM )");
		inspector.setFormat("DSSCS");
		format = inspector.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/dsscs.txt"))));
		assertEquals("DSSCS", format);
	}
	
	@Test
	public void testInspectNom() throws IOException {
		inspector.setPattern("(gov:ic:nro:om)");
		inspector.setFormat("NOM");
		format = inspector.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/nom.xml"))));
		assertEquals("NOM", format);
	}
		
	@Test
	public void testInspectSocomm() throws IOException{
		inspector.setPattern("(BT)");
		inspector.setFormat("SOCOMM");
		format = inspector.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/socomm.txt"))));
		assertEquals("SOCOMM", format);
	}
}
