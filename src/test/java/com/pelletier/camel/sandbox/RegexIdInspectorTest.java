package com.pelletier.camel.sandbox;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.pelletier.camel.sandbox.RegexIdInspector;

import static org.junit.Assert.assertEquals;


public class RegexIdInspectorTest {
	
	int groupNumber;
	String id = null;
	RegexIdInspector inspector = null;
	
	@Before
	public void setup(){
		inspector = new RegexIdInspector();
	}

	@Test
	public void testInspectIdFailure() throws IOException {
		inspector.setPattern("([0-9]{4})\\.([0-9]{4})"); //ID grouped into left of "." and right of "."
		id = inspector.inspect("looking for an id in this sentence".getBytes());
		assertEquals(null, id);
	}
	
	@Test
	public void testInspectLeftId() throws IOException {
		inspector.setPattern("([0-9]{4})\\.([0-9]{4})"); //ID grouped into left of "." and right of "."
		id = inspector.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/dsscs.txt"))));
		assertEquals("1200", id);
	}
	
	@Test
	public void testInspectRightId() throws IOException {
		inspector.setPattern("([0-9]{4})\\.([0-9]{4})"); //ID grouped into left of "." and right of "."
		inspector.setGroupNumber(2);
		id = inspector.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/nom.xml"))));
		assertEquals("0001", id);
	}
	
	@Test
	public void testInspectWholeId() throws IOException {
		inspector.setGroupNumber(1);
		inspector.setPattern("([0-9]{4}\\.[0-9]{4})");	//entire ID as group
		id = inspector.inspect(IOUtils.toByteArray(FileUtils.openInputStream(new File("testFiles/socomm.txt"))));
		assertEquals("1111.1111", id);
	}
	
}
