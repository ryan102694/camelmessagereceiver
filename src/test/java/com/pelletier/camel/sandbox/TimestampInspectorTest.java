package com.pelletier.camel.sandbox;

import static org.junit.Assert.*;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;

public class TimestampInspectorTest {
	
	TimestampInspector timestampInspector = null;
	byte[] message;
	
	@Before
	public void setup(){
		timestampInspector = new TimestampInspector();
		timestampInspector.setSimpleDateFormatPattern("yyyy-MM-dd_hhmm-aaa");
		message = new String("here is a sample message").getBytes();
	}

	@Test
	public void testTestBeginTimeDoesNotChangeWhenMessagesArriveWithinInterval() throws InterruptedException {
		timestampInspector.setMaxTimeBetweenMessages(5 * 1000);	//5 seconds
		
		Date testBeginReference = timestampInspector.getTestBeginTime();
		timestampInspector.inspect(message);
		Thread.sleep(300);
		timestampInspector.inspect(message);
		Thread.sleep(100);
		timestampInspector.inspect(message);
		Thread.sleep(200);
		timestampInspector.inspect(message);
		
		//total wait is less than 5 seconds, the testTimeBegin should be the same as it was when we started
		assertEquals(testBeginReference.getTime(), timestampInspector.getTestBeginTime().getTime());
	}
	
	@Test
	public void testTestBeginTimeChangesWhenMessagesArriveOutsideInterval() throws InterruptedException {
		timestampInspector.setMaxTimeBetweenMessages(1000);	//1 second
		
		Date testBeginReference = timestampInspector.getTestBeginTime();	
		timestampInspector.inspect(message);
		Thread.sleep(1100);
		timestampInspector.inspect(message);
		
		//wait between messages is greater than 1 second, testTimeBegin has been updated
		assertNotEquals(testBeginReference.getTime(), timestampInspector.getTestBeginTime().getTime());
	}
}
